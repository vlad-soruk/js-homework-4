"use strict"

// Отримати за допомогою модального вікна браузера два числа //
let numberOne = prompt("Enter first number:");
let firstNumber = Number(numberOne);
let defaultFirstNumber = "";

// Перевірка коректності введенних даних //
while (isNaN(firstNumber) || firstNumber == "") {
    defaultFirstNumber = numberOne;
    numberOne = prompt("Sorry. The number is incorrect. Enter the proper number:", defaultFirstNumber);
    firstNumber = Number(numberOne);
}

let numberTwo = prompt("Enter second number:");
let secondNumber = Number(numberTwo);

// Перевірка коректності введенних даних //
while (isNaN(secondNumber) || secondNumber == "") {
    numberOne = prompt("Sorry. The number is incorrect. Enter the FIRST number:");
    firstNumber = Number(numberOne);
    while (isNaN(firstNumber) || firstNumber == "") {
        defaultFirstNumber = numberOne;
        numberOne = prompt("Sorry. The number is incorrect. Enter the FIRST number:", defaultFirstNumber);
        firstNumber = Number(numberOne);
    }
    numberTwo = prompt("Now enter the SECOND number:");
    secondNumber = Number(numberTwo);
}

// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати //
// Сюди може бути введено +, -, *, /. //
// Плюс перевірка коректності введенних даних //
let operation = prompt("Enter a math operation. (+, -, * or /)");
while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    operation = prompt("Incorrect operation. Try again: (+, -, * or /)");
}

// Створити функцію, в яку передати два значення та операцію //
// Вивести у консоль результат виконання функції //
function mathOperation(firstNumber, secondNumber, operation) {
    if (operation === "+") {
        let result = firstNumber + secondNumber;
        return result;
    }
    else if (operation === "-") {
        let result = firstNumber - secondNumber;
        return result;
    }
    else if (operation === "*") {
        let result = firstNumber * secondNumber;
        return result;
    }
    else {
        let result = firstNumber / secondNumber;
        return result;
    }
}

console.log(mathOperation(firstNumber, secondNumber, operation));

